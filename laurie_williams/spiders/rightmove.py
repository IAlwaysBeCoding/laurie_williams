
import json
import os
import re

from furl import furl
from scrapy import Spider, Selector, Request
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, TakeFirst, MapCompose, Join, SelectJmes

from laurie_williams.items import RightmoveListingItem, RightmovePropertyMeta
from laurie_williams.utils import default_missing_keys, strip_spaces, NoneEmpty, TakeN, Found



class RightmoveException(Exception):
    pass

class RightmoveSpider(Spider):
    name = 'rightmove'
    allowed_domains = ['rightmove.co.uk']
    start_urls = ['http://rightmove.co.uk/']

    def __init__(self, *args, **kwargs):

        self.query = kwargs.get('query', '')
        self.radius = kwargs.get('radius', '0.0')
        self.min_bedrooms = kwargs.get('min_bedrooms', '')
        self.max_bedrooms = kwargs.get('max_bedrooms', '')
        self.min_price = kwargs.get('min_price', '')
        self.max_price = kwargs.get('max_price', '')
        self.property_type = kwargs.get('property_type', '')
        self.added_to_site = kwargs.get('added_to_site', '')


    def start_requests(self):

        meta = {}
        meta['search_meta'] = {
            'query' : self.query,
            'radius' : self.radius,
            'min_bedrooms' : self.min_bedrooms,
            'max_bedrooms' : self.max_bedrooms,
            'min_price' : self.min_price,
            'max_price' : self.max_price,
            'property_type' : self.property_type,
            'added_to_site' : self.added_to_site,
        }

        yield self._build_search_query_stations_request(self.query, meta)

    def _extract_property_id(self, response):

        property_id = response.xpath('//link[@rel="canonical"]/@href').extract_first()
        if property_id:
            get_property_id = re.search(r'(?<=property-)(.*?)(?=.html)', property_id, re.DOTALL)

            if get_property_id:
                return get_property_id.group(0)

    def _extract_geolocation(self, response):

        find_geolocation = re.search(r'(?<=)latitude":(.*?),"longitude":(.*?)(?:})', str(response.body), re.DOTALL)

        if find_geolocation:
            return [find_geolocation.group(1), find_geolocation.group(2)]
        return []

    def _extract_nearest_stations(self, response):

        stations = []

        for station_li in response.xpath('//ul[@class="stations-list"]/li').extract():
            station_li_sel = Selector(text=station_li)
            name = station_li_sel.xpath('//span/text()').extract_first()
            distance = station_li_sel.xpath('//small/text()').extract_first()
            icons = station_li_sel.xpath('//i/@class').extract()

            stations.append({
                'name' : name,
                'distance' : distance,
                'stationType' : icons
            })

        return stations

    def _build_search_query_stations_request(self, query, meta):

        f = furl('http://www.rightmove.co.uk/typeAhead/uknostreet')

        for i in range(1, 10, 2):
            if len(query) <= i-1:
                break
            elif len(query) == i:
                f._path.segments.append(query[i-1].upper())
            elif len(query) < i:
                break
            else:
                f._path.segments.append(query[i-1].upper() + query[i].upper())

        return Request(f.url,
                      meta=meta,
                      callback=self.parse_query_stations)

    def _build_search_page_request(self, query, station_id, meta):

        f = furl('http://www.rightmove.co.uk/property-for-sale/search.html')

        station_number = station_id.split('^')[0]
        query = {
            'searchLocation' : query,
            'useLocationIdentifier' : 'false',
            'locationIdentifier' : '',
            'buy.x' : 'SALE',
            'search' : 'Start Search',
            'topMatchPersistRegIds' : station_number,
            'lastPersistLocId' : station_id,
        }

        f.set(query)

        return Request(f.url,
                      meta=meta,
                      callback=self.parse_search_page)

    def _build_search_detail_page_request(self, response):


        meta = response.meta
        f = furl('http://www.rightmove.co.uk/property-for-sale/find.html')

        station_id = response.meta['search_meta']['station_id']
        query = {
            'searchType' : 'SALE',
            'locationIdentifier' : meta['search_meta']['station_id'],
            'insId' : '1',
            'radius' : meta['search_meta']['radius'],
            'minPrice' : meta['search_meta']['min_price'],
            'maxPrice' : meta['search_meta']['max_price'],
            'minBedrooms' : meta['search_meta']['min_bedrooms'],
            'maxBedrooms' : meta['search_meta']['max_price'],
            'displayPropertyType' : meta['search_meta']['property_type'],
            'maxDaysSinceAdded' : meta['search_meta']['added_to_site'],
            '_includeSSTC' : 'on',
            'sortByPriceDescending' : '',
            'primaryDisplayPropertyType' : '',
            'secondaryDisplayPropertyType' : '',
            'oldDisplayPropertyType' : '',
            'oldPrimaryDisplayPropertyType' : '',
            'newHome' : '',
            'auction' : 'false',
            'index' : meta['search_meta']['index'],
        }
        f.set(query)

        return Request(f.url,
                       meta={'search_meta' : meta['search_meta']},
                       callback=self.parse_search_results_page)

    def _build_nearby_request(self, search_type, property_id, meta):

        if search_type == 'sold':
            f = furl('http://www.rightmove.co.uk/ajax/house-prices/similar-sold.html')
            f.args['propertyId'] = property_id

        elif search_type in ('under-offer', 'for-sale'):
            f = furl('http://www.rightmove.co.uk/ajax/house-prices/similar-properties.html')
            f.args['searchType'] = search_type
            f.args['propertyId'] = property_id

        elif search_type in ('primary', 'secondary'):
            f = furl('http://www.rightmove.co.uk/ajax/schools/property')
            f.args['ageGroupType'] = search_type
            f._path.segments.append(property_id)

        url = f.url
        meta['search_type'] = search_type

        headers = {
            'X-Requested-With' : 'XMLHttpRequest'
        }
        return Request(
            url,
            meta=meta,
            headers=headers,
            callback=self.parse_nearby_results
        )

    def _build_meta_dict(self, response):

        property_id = response.meta.get('property_id', None)
        main_page_response = response.meta.get('main_page_response', None)
        featured_listing = response.meta.get('featured_listing', False)
        premium_listing = response.meta.get('premium_listing', False)
        search_type = response.meta.get('search_type', None)
        group_type = response.meta.get('group_type', None)
        nearby_results = response.meta.get('nearby_results', {})
        nearby_jmes_query = response.meta.get('nearby_jmes_query', None)
        nearby_schools_primary_jmes_query = response.meta.get('nearby_schools_primary_jmes_query', None)
        nearby_schools_secondary_jmes_query = response.meta.get('nearby_schools_secondary_jmes_query', None)
        next_nearby_request = response.meta.get('next_nearby_request', None)

        return {
            'property_id' : property_id,
            'main_page_response' : main_page_response,
            'featured_listing' : featured_listing,
            'premium_listing' : premium_listing,
            'search_type' : search_type,
            'group_type' : group_type,
            'nearby_results' : nearby_results,
            'nearby_jmes_query' : nearby_jmes_query,
            'nearby_schools_primary_jmes_query' : nearby_schools_primary_jmes_query,
            'nearby_schools_secondary_jmes_query' : nearby_schools_secondary_jmes_query,
            'next_nearby_request' : next_nearby_request,
        }

    def _build_all_nearby_requests(self, response):

        property_id = self._extract_property_id(response)
        if not property_id:
            raise RightmoveException('Could not parse propertyid')

        response.meta['property_id'] = property_id

        meta = self._build_meta_dict(response)
        meta['nearby_jmes_query'] = '''views[].{"listingId" : propertyId, ''' \
           '''"listingPrice" : "price", "listingAddress" : address, ''' \
           '''"listingDistance" : distance}'''

        meta['nearby_schools_primary_jmes_query'] = '''schools[].{"schoolGuideUrl" : ''' \
            '''schoolGuideUrl, "type" : type, "specificType" : specificType "name" ''' \
            ''': name, "distance" : distance, "tier" : "primary", "position" : null, ''' \
            '''"schoolRating" : schoolRating, "schoolSubscription" : schoolSubscription, ''' \
            '''"latitude" : latitudeLongitude.lat, "longitude" : latitudeLongitude.lon, ''' \
            '''"minimumAge" : minimumAge, "maximumAge" : maximumAge, "pupilCount" : pupilCount, ''' \
            '''"gender" : gender, "religion" : religion}'''

        meta['nearby_schools_secondary_jmes_query'] = '''schools[].{"schoolGuideUrl" : ''' \
            '''schoolGuideUrl, "type" : type, "specificType" : specificType "name" ''' \
            ''': name, "distance" : distance, "tier" : "secondary", "position" : null, ''' \
            '''"schoolRating" : schoolRating, "schoolSubscription" : schoolSubscription, ''' \
            '''"latitude" : latitudeLongitude.lat, "longitude" : latitudeLongitude.lon, ''' \
            '''"minimumAge" : minimumAge, "maximumAge" : maximumAge, "pupilCount" : pupilCount, ''' \
            '''"gender" : gender, "religion" : religion}'''



        nearby_requests = []
        search_types = []

        search_types.extend(['for-sale', 'under-offer', 'sold', 'primary', 'secondary'])
        nearby_requests.extend([
            self._build_nearby_request(search_type, property_id, meta)
            for search_type in search_types
        ])


        for i, nearby_request in enumerate(nearby_requests):

            if len(nearby_requests) == i+1:
                nearby_request.meta['next_nearby_request'] = None
            else:
                nearby_request.meta['next_nearby_request'] = nearby_requests[i+1]

        yield nearby_requests[0]


    def parse_query_stations(self, response):

        meta = {}
        meta['search_meta'] = response.meta['search_meta']
        query = meta['search_meta']['query']
        data = json.loads(response.body)
        parse = SelectJmes('''typeAheadLocations[?displayName=='Waterloo Station'].locationIdentifier | [0]''')

        station_id = parse(data)
        if station_id:
            meta['search_meta']['station_id'] = station_id
            yield self._build_search_page_request(query, station_id, meta)

    def parse_nearby_results(self, response):

        f = furl(response.url)
        meta = self._build_meta_dict(response)

        search_type = meta.get('search_type', '')

        if search_type in ('for-sale', 'under-offer', 'sold'):
            jmes_query = response.meta['nearby_jmes_query']
        elif search_type == 'primary':
            jmes_query = response.meta['nearby_schools_primary_jmes_query']
        elif search_type == 'secondary':
            jmes_query = response.meta['nearby_schools_secondary_jmes_query']

        data = json.loads(response.body)
        parse = SelectJmes(jmes_query)

        result = parse(data)
        next_request = meta['next_nearby_request']
        if next_request:

            next_request.meta['nearby_results'][search_type] = result
            return next_request

        else:

            meta['main_page_response'].meta.update(meta)
            meta['main_page_response'].meta['nearby_results'][search_type] = result
            return self.parse_main_listing_page(meta['main_page_response'])

    def parse_main_listing_page(self, response):

        meta = self._build_meta_dict(response)
        loader = ItemLoader(RightmoveListingItem(), response=response)

        loader.default_input_processor = MapCompose(strip_spaces)
        loader.default_output_processor = NoneEmpty()

        loader.listingImages_out = Identity()
        loader.listingFeatures_out = Identity()
        loader.listingGeolocation_out = Identity()
        loader.nearbyForSale_out = Identity()
        loader.nearbySold_out = Identity()
        loader.nearbyUnderOffer_out = Identity()
        loader.nearbySchools_out = Identity()
        loader.nearestStations_out = Identity()

        loader.newHome_in = Found()

        loader.add_xpath('listingPrice', '//p[@id="propertyHeaderPrice"]//text()')
        loader.add_xpath('listingTitle', '//div[contains(@class,"property-header-bedroom-and-price")]//h1[@itemprop="name"]/text()')
        loader.add_xpath('listingAddress', '//div[contains(@class,"property-header-bedroom-and-price")]//meta[@itemprop="streetAddress"]/@content')
        loader.add_xpath('listingImages', '//li[contains(@class, "js-gallery-thumbnail")]/meta[@itemprop="contentUrl"]/@content')
        loader.add_xpath('listingFeatures', '//ul[@class="list-two-col list-style-square"]//li//text()')
        loader.add_xpath('listingDescription', '//p[@itemprop="description"]/text()')
        loader.add_xpath('listingGeolocation', self._extract_geolocation(response))
        loader.add_xpath('listingAdded', '//div[@id="firstListedDateValue"]/text()')
        loader.add_value('nearbyForSale', meta['nearby_results']['for-sale'])
        loader.add_value('nearbyUnderOffer', meta['nearby_results']['under-offer'])
        loader.add_value('nearbySold', meta['nearby_results']['sold'])
        loader.add_value('nearbySchools', meta['nearby_results']['primary'])
        loader.add_value('nearbySchools', meta['nearby_results']['secondary'])
        loader.add_value('nearestStations', self._extract_nearest_stations(response))
        loader.add_value('saleHistory', '')
        loader.add_xpath('newHome','//strong[@class="ribbon-channel ribbon-channel-new"]', )
        loader.add_xpath('priceSubtitle','//p[@id="propertyHeaderPrice"]//text()')
        loader.add_value('premiumListing', meta['premium_listing'])
        loader.add_value('featuredListing', meta['featured_listing'])

        listing_page_item = loader.load_item()
        default_missing_keys(listing_page_item, default_value='')

        return listing_page_item

    def parse_listing_page(self, response):

        response.meta['main_page_response'] = response

        for nearby_request in self._build_all_nearby_requests(response):
            yield nearby_request

    def parse_search_page(self, response):

        response.meta['search_meta']['index'] = response.meta['search_meta'].get('index', 0)
        return self._build_search_detail_page_request(response)


    def parse_search_results_page(self, response):

        props_raw = response.xpath('//div[contains(@class,"l-searchResult is-list")]').extract()
        found_results = False
        for prop_raw in props_raw:
            prop_sel = Selector(text=prop_raw)


            get_property_id = prop_sel.xpath('//a[starts-with(@id,"prop")]/@id').extract_first()
            property_url = prop_sel.xpath('//a[contains(@class, "propertyCard-img-link")]/@href').extract_first()

            if get_property_id and property_url:
                found_results = True
                property_id = get_property_id.replace('prop','')

                is_premium = response.xpath('//div[contains(@class, "propertyCard--premium")]').extract_first()
                is_featured= response.xpath('//div[contains(@class, "propertyCard--featured")]').extract_first()

                response.meta['search_meta']['property_id'] = property_id
                response.meta['search_meta']['property_url'] = response.urljoin(property_url)

                yield Request(response.urljoin(property_url),
                              meta={
                                  'search_meta' : response.meta['search_meta'],
                                  'premium_listing' : True if is_premium else False,
                                  'featured_listing' : True if is_featured else False,
                                  'property_request' : True
                              },
                              callback=self.parse_listing_page)


        if found_results:

            response.meta['search_meta']['index'] = response.meta['search_meta']['index'] + 24
            yield self._build_search_detail_page_request(response)







