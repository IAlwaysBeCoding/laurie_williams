from scrapy import Item, Field

class RightmovePropertyMeta(Item):

    query = Field()
    radius = Field()
    min_bedrooms = Field()
    max_bedrooms = Field()
    min_price = Field()
    max_price = Field()
    property_type = Field()
    added_to_site = Field()
    station_id = Field()
    property_id = Field()
    property_url = Field()
    index = Field()


class RightmoveListingItem(Item):

    listingPrice = Field()
    listingTitle = Field()
    listingImages = Field()
    listingAddress = Field()
    listingFeatures = Field()
    listingDescription = Field()
    listingGeolocation = Field()
    listingAdded = Field()
    nearbyForSale = Field()
    nearbyUnderOffer = Field()
    nearbySchools = Field()
    nearbySold = Field()
    nearestStations = Field()
    saleHistory = Field()
    newHome = Field()
    priceSubtitle = Field()
    premiumListing = Field()
    featuredListing = Field()
