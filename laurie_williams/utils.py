import sys

from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose


def default_missing_keys(item, default_value, except_keys=[],missing_key_defaults={}):

    missing_keys = list(set(item.fields.keys()) - set(item.keys()))
    for missing_key in missing_keys:
        if except_keys:
            if missing_key not in except_keys:
                item[missing_key] = default_value
        else:
            item[missing_key] = default_value

        if missing_key in missing_key_defaults:
            item[missing_key] = missing_key_defaults[missing_key]

def strip_spaces(raw_str):

    str_types = (str) if sys.version_info[0] == 3 else (unicode, str)

    if isinstance(raw_str,  str_types):
        return raw_str.strip().replace('\n', ' ').replace('\t', '')
    else:
        return raw_str

class Found(object):

    def __init__(self, found_value=True, not_found_value=False):
        self.found_value = found_value
        self.not_found_value = not_found_value

    def __call__(self, values):
        return self.found_value if values else self.not_found_value

class NoneEmpty(object):
    def __call__(self,values):
        if values is None:
            return ''
        elif isinstance(values, (list, tuple)):
            if len(values) == 1 and values[0] == '':
                return ''
        return TakeFirst()(values)

class Replace(object):
    def __init__(self, find_str, rep_str):
        self.find_str = find_str
        self.rep_str = rep_str

    def __call__(self, value):
        return value.replace(self.find_str, self.rep_str)

class Prefix(object):
    def __init__(self, prefix_str):
        self.prefix_str = prefix_str

    def __call__(self, value):
        return '{}{}'.format(self.prefix_str, value)

class Suffix(object):
    def __init__(self,suffix_str):
        self.suffix_str = suffix_str

    def __call__(self, value):
        return '{}{}'.format(value, self.suffix_str)

class TakeN(object):
    def __init__(self, index, default=''):
        self.index = index
        self.default = default

    def __call__(self, values):
        try:
            return values[self.index]
        except:
            return self.default

class SplitTakeIndex(object):
    def __init__(self, split_str, index, default=''):
        self.split_str = split_str
        self.index = index
        self.default = default

    def __call__(self, values):
        try:
            return values.split(self.split_str)[self.index]
        except:
            return self.default

class TakeOnlyIndexes(object):

    def __init__(self, indexes):

        self.indexes = indexes

    def __call__(self, values):

        filtered_values = []

        for i, value in enumerate(values):
            if i in self.indexes:
                filtered_values.append(value)

        return filtered_values


